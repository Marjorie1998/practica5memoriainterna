package conforme.marjorie.pm4a.practica;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity   {
    TextView cedula, nombre, apellidos, datos;
    Button botonEscribir, botonLeer;

    Button config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cedula =(TextView) findViewById(R.id.txtcedula);
        nombre =(TextView) findViewById(R.id.txtnombre);
        apellidos =(TextView) findViewById(R.id.txtapellido);
        datos = (TextView)findViewById(R.id.txtDatosMI);
        botonEscribir =(Button) findViewById(R.id.btnescr);
        botonLeer = (Button)findViewById(R.id.btnleer);

        botonEscribir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivo.txt", Context.MODE_PRIVATE));
                    escritor.write(cedula.getText().toString()+
                            ","+apellidos.getText().toString()+
                            ","+nombre.getText().toString());
                    Log.e("Archivo MI","Error en el archivo de escritura");
                    escritor.close();

                }catch (Exception ex){

                    Log.e("Archivo MI","Error en el archivo de escritura");
                }


            }
        });
        botonLeer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    BufferedReader read = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String texto = read.readLine();
                    datos.setText(texto);
                    read.close();


                   /* BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String dato = lector.readLine();
                    String [] listaPersonas = dato.split(";");
                    for(int i = 0; i < listaPersonas.length; i++){

                        datos.append(listaPersonas[i].split(",")[0] + " " +  listaPersonas[i].split(",")[1] + " " +
                                listaPersonas[i].split(",")[2]);
                    }

                    // cajaDatos.setText("");
                    lector.close();*/

                }catch (Exception ex){

                    Log.e("Archivo MI","Error en la lectura del archivo" );//+ ex.getMessage());
                }

            }
        });



        config=(Button)findViewById(R.id.btnconfig);

        config.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent= new Intent(MainActivity.this, Configuracion.class);
                //
               // Bundle bundle=new Bundle();
              //  bundle.putString("dato",datos.getText().toString());
                //intent.putExtras(bundle);
                //

                startActivity(intent);
            }
        });

    }

   /* @Override
    public void onClick(View v) {
        //codigo escritura
        switch (v.getId()){

            case R.id.btnescr:

                try {
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivo.txt", Context.MODE_APPEND));
                    escritor.write(cedula.getText().toString()+
                            ","+apellidos.getText().toString()+
                            ","+nombre.getText().toString());
                    escritor.close();

                }catch (Exception ex){

                    Log.e("Archivo MI","Error en el archivo de escritura");
                }
                break;
            case R.id.btnleer:





                try {

                    BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String dato = lector.readLine();
                    String [] listaPersonas = dato.split(";");
                    for(int i = 0; i < listaPersonas.length; i++){

                       datos.append(listaPersonas[i].split(",")[0] + " " +  listaPersonas[i].split(",")[1] + " " +
                                listaPersonas[i].split(",")[2]);
                    }

                    // cajaDatos.setText("");
                    lector.close();

                }catch (Exception ex){

                    Log.e("Archivo MI","Error en la lectura del archivo" + ex.getMessage());
                }
                break;
        */
}


